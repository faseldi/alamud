from .action import Action2
from mud.events import SpeakEvent

class SpeakAction(Action2):
    EVENT = SpeakEvent
    RESOLVE_OBJECT = "resolve_for_take"
    ACTION = "speak"