from .event import Event2


class EatEvent(Event2):
    NAME = "eat"

    def perform(self):
        self.object.move_to(None)
        self.inform("eat")


