---
id: badge-000
type: Thing
name: badge
props:
  - takable
  - key-for-porte-porche-000
events:
  info:
    actor: "Un badge de sécurité."
  look:
    actor: |
      C'est un badge de sécurité permettant d'ouvrir des
      portes sécurisées.
      
---
id: briquet-000
type: Thing
props:
  - lightable
  - takable
names   :
  - briquet
  - zipo
events:
  info:
    actor:
      - props: =briquet-000:light-on
        data : "Un briquet allumé."
      - data : "Un briquet éteint."
  look:
    actor:
      - props: object:light-on
        data : "Un briquet allumé."
      - data : "Un briquet éteint."
  light-on:
    - props : object:light-on
      actor : "Le briquet est déjà allumée."
    - actor : "Vous allumez votre briquet."
      observer: |
        {{ actor.name }} allume son briquet.
      effects:
        - type  : ChangePropEffect
          modifs: object:light-on
  light-off:
    - props : -object:light-on
      actor : "Le briquet est déjà éteinte."
    - actor : "Vous éteignez votre briquet."
      observer: |
        {{ actor.name }} éteind son briquet.
      effects:
        - type  : ChangePropEffect
          modifs: -object:light-on
---
id: flambeau-000
type: Thing
props:
  - ignitable
  - takable
names   :
  - flambeau
events:
  info:
    actor:
      - props: =flambeau-000:light-on
        data : "Un flambeau allumé."
      - data : "Un flambeau éteint."
  look:
    actor:
      - props: object:light-on
        data : "Un flambeau allumé."
      - data : "Un flambeau éteint."
  light-on:
    - props : object:light-on
      actor : "Le flambeau est déjà allumée."
    - actor : "Vous allumez votre flambeau."
      observer: |
        {{ actor.name }} allume son flambeau.
      effects:
        - type  : ChangePropEffect
          modifs: object:light-on
  light-off:
    - props : -object:light-on
      actor : "Le flambeau est déjà éteinte."
    - actor : "Vous éteignez votre flambeau."
      observer: |
        {{ actor.name }} éteind son flambeau.
      effects:
        - type  : ChangePropEffect
          modifs: -object:light-on
          
